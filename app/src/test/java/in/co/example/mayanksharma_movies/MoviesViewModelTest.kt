package `in`.co.example.mayanksharma_movies

import `in`.co.example.mayanksharma_movies.common.BaseTestClass
import `in`.co.example.mayanksharma_movies.domain.AppResult
import `in`.co.example.mayanksharma_movies.domain.usecases.InternetUseCase
import `in`.co.example.mayanksharma_movies.domain.usecases.MoviesUseCase
import `in`.co.example.mayanksharma_movies.presentation.movies.MovieRowData
import `in`.co.example.mayanksharma_movies.presentation.movies.MoviesViewModel
import `in`.co.example.mayanksharma_movies.testResponse.MovieListResponse.moviesListTestResponse
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


class MoviesViewModelTest : BaseTestClass() {

    private lateinit var testMoviesViewModelTest: MoviesViewModel

    @Mock
    private lateinit var moviesUseCase: MoviesUseCase

    @Mock
    private lateinit var internetUseCase: InternetUseCase


    @Before
    override fun setUp() {
        testMoviesViewModelTest = MoviesViewModel(moviesUseCase,internetUseCase)
    }

    @Test
    fun getAllMoviesList(){
        val moviesListTestResponse = moviesListTestResponse()
        val testResponse = AppResult.Success(moviesListTestResponse)
        var actualResponse : List<MovieRowData>? = null

        val lock = CountDownLatch(4)

        testMoviesViewModelTest.moviesLiveData.observeForever{
            actualResponse = it
        }

        runBlockingTest {
            Mockito.`when`(
                moviesUseCase.getMoviesList()
            ).thenReturn(testResponse)
            testMoviesViewModelTest.getMoviesData()
        }

        lock.await(timeOut,TimeUnit.MILLISECONDS)

        val actualTitle = (actualResponse?.get(0) as MovieRowData).movieName
        assertEquals("I'm Your Man",actualTitle)
    }

}