package `in`.co.example.mayanksharma_movies.testResponse

import `in`.co.example.mayanksharma_movies.domain.entities.Movie
import `in`.co.example.mayanksharma_movies.domain.entities.MoviesEntity

object MovieListResponse  {

   fun moviesListTestResponse():MoviesEntity{

       val result : MutableList<Movie> = mutableListOf()

      val movie =  Movie(
          displayTitle = "I'm Your Man",
          rating = "R",
          criticsPick = 1,
          byLine = "Jeannette Catsoulis",
          headLine = "‘I’m Your Man’ Review: Living Doll",
          summaryShort = "Dan Stevens plays a dreamy, pleasure-driven android in this delightful near-future romance.",
          publicationDate = "2021-09-23",
          openingDate = null,
          dateUpdated = null,
          link = null,
          multimedia = null
      )

       result.add(movie)

       return MoviesEntity(
           status = "success",
           copyright = "Copyright (c) 2021 The New York Times Company. All Rights Reserved.",
           hasMore = false,
           num_results = 1,
           movies = result
       )
   }

}