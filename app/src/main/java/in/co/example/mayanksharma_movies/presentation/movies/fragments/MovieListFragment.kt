package `in`.co.example.mayanksharma_movies.presentation.movies.fragments

import `in`.co.example.mayanksharma_movies.R
import `in`.co.example.mayanksharma_movies.common.base.BaseFragment
import `in`.co.example.mayanksharma_movies.databinding.MoviesListFragmentBinding
import `in`.co.example.mayanksharma_movies.databinding.SnackErrorViewBinding
import `in`.co.example.mayanksharma_movies.domain.SnackErrorType
import `in`.co.example.mayanksharma_movies.presentation.movies.MoviesActivity
import `in`.co.example.mayanksharma_movies.presentation.movies.MoviesListAdapter
import `in`.co.example.mayanksharma_movies.presentation.movies.MoviesViewModel
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

class MovieListFragment : BaseFragment<MoviesListFragmentBinding>(
    R.layout.movies_list_fragment
) {

    private val moviesViewModel: MoviesViewModel by activityViewModels { viewModelFactory }
    private val moviesAdapter: MoviesListAdapter by lazy {
        MoviesListAdapter(onMoviesItemClick)
    }

    override fun getSnackBarErrorView(): SnackErrorViewBinding = viewBinding.includeErrorView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initHeader()
        initRecyclerView()
        initData()
        setUpObservers()
    }

    private fun initHeader() {
        (activity as MoviesActivity).supportActionBar?.title = getString(R.string.movies_label)

    }

    private fun initRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        viewBinding.rvMovies.apply {
            layoutManager = linearLayoutManager
            adapter = moviesAdapter
        }
    }

    private fun initData() {
        moviesViewModel.getMoviesData()
        moviesViewModel.registerInternetStatus()
    }

    private fun setUpObservers() {
        moviesViewModel.moviesLiveData.observe(viewLifecycleOwner, Observer {
            moviesAdapter.addItems(it)
            hideSnackBarErrorView()
        })

        moviesViewModel.errorLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is SnackErrorType.InternetError -> {
                    if (it.isInternet) {
                        hideSnackBarErrorView()
                    } else {
                        showSnackBarErrorView(getString(R.string.device_offline_error_message))
                    }
                }

                is SnackErrorType.ServerError -> {

                }
            }
        })

        moviesViewModel.loaderLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                true -> viewBinding.loaderView.visibility = View.VISIBLE
                false -> viewBinding.loaderView.visibility = View.GONE
            }
        })
    }

    private val onMoviesItemClick = fun(position: Int) {
        val bundle = Bundle()
        bundle.putInt("position", position)
        findNavController().navigate(R.id.action_MovieFragment_to_MovieDetailFragment, bundle)
    }
}