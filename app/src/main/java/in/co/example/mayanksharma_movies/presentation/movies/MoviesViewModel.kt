package `in`.co.example.mayanksharma_movies.presentation.movies

import `in`.co.example.mayanksharma_movies.domain.AppResult
import `in`.co.example.mayanksharma_movies.domain.SnackErrorType
import `in`.co.example.mayanksharma_movies.domain.entities.Movie
import `in`.co.example.mayanksharma_movies.domain.usecases.InternetUseCase
import `in`.co.example.mayanksharma_movies.domain.usecases.MoviesUseCase
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MoviesViewModel(
    private val moviesUseCase: MoviesUseCase,
    private val internetUseCase: InternetUseCase
) : ViewModel() {

    private var movies: MutableList<Movie> = mutableListOf()

    private val _errorLiveData = MutableLiveData<SnackErrorType>()
    val errorLiveData: LiveData<SnackErrorType> get() = _errorLiveData

    private val _loaderLiveData = MutableLiveData<Boolean>()
    val loaderLiveData: LiveData<Boolean> get() = _loaderLiveData


    private val _moviesLiveData = MutableLiveData<List<MovieRowData>>()
    val moviesLiveData: LiveData<List<MovieRowData>> get() = _moviesLiveData


    fun getMoviesData() {
        viewModelScope.launch(Dispatchers.IO) {
            _loaderLiveData.postValue(true)
            when (val result = moviesUseCase.getMoviesList()) {
                is AppResult.Success -> {
                    result.data?.movies?.let {
                        movies.clear()
                        movies.addAll(it)
                    }
                    _moviesLiveData.postValue(result.data?.movies?.let { renderMovieRowData(it) })
                    _loaderLiveData.postValue(false)
                }

                is AppResult.Failure -> {
                    _loaderLiveData.postValue(false)
                    result.error.message?.let {
                        _errorLiveData.value = SnackErrorType.ServerError(it)
                    }
                }
            }
        }
    }

    fun registerInternetStatus(){
        viewModelScope.launch {
            internetUseCase.receiveInternetStatus().collect { internetAvailable ->
                _errorLiveData.postValue(
                    SnackErrorType.InternetError(
                        internetAvailable
                    )
                )
            }
        }
    }

    fun getMovieDetail(itemPosition: Int): MovieDetail {
        val movie = movies[itemPosition]

        return MovieDetail(
            name = movie.displayTitle ?: "",
            headLine = movie.headLine ?: "",
            description = movie.summaryShort ?: "",
            rating = movie.rating ?: "NA",
            by = movie.byLine ?: "NA",
            havingArticle = isItemHavingArticle(movie),
            articleLink = movie.link?.url ?: "",
            articleHeading = movie.link?.suggestedLinkText ?: "",
            image = movie.multimedia?.src ?: ""
        )
    }

    private fun isItemHavingArticle(movie: Movie): Boolean {
        return movie.link?.let {
            it.type.equals("article", true)
        } ?: false
    }

    private suspend fun renderMovieRowData(movies: List<Movie>): List<MovieRowData> {
        return withContext(Dispatchers.IO){

            val newDataList: MutableList<MovieRowData> = mutableListOf()
            for (movie in movies) {
                newDataList.add(
                    MovieRowData(
                        movieName = movie.displayTitle ?: "",
                        rating = movie.rating ?: "",
                        movieImage = movie.multimedia?.src ?: ""
                    )
                )
            }
             newDataList
        }
    }

}