package `in`.co.example.mayanksharma_movies.data.repository

import `in`.co.example.mayanksharma_movies.data.sources.api.MoviesApi
import `in`.co.example.mayanksharma_movies.data.sources.auth.AuthSource
import `in`.co.example.mayanksharma_movies.domain.AppResult
import `in`.co.example.mayanksharma_movies.domain.entities.MoviesEntity

interface MoviesRepository {
    suspend fun getMoviesList(): AppResult<MoviesEntity?>
}

class MoviesRepositoryImpl(private val moviesApi: MoviesApi) : MoviesRepository {

    override suspend fun getMoviesList(): AppResult<MoviesEntity?> {
        return moviesApi.getMoviesList()
    }

}