package `in`.co.example.mayanksharma_movies.common.di.providers

import `in`.co.example.mayanksharma_movies.data.repository.InternetRepository
import `in`.co.example.mayanksharma_movies.data.repository.InternetRepositoryImpl
import `in`.co.example.mayanksharma_movies.data.repository.MoviesRepository
import `in`.co.example.mayanksharma_movies.data.repository.MoviesRepositoryImpl

class RepositoryProvider(
    apiSourceProvider: ApiSourceProvider,
    internetSourceProvider: InternetSourceProvider
) {

  /*----------------------------------------Private----------------------------------------*/

    private val moviesRepository: MoviesRepository by lazy {
        MoviesRepositoryImpl(
            apiSourceProvider.provideMoviesApi()
        )
    }

    private val internetRepository: InternetRepository by lazy {
        InternetRepositoryImpl(
            internetSourceProvider.provideInternetSource()
        )
    }


    /*----------------------------------------Public----------------------------------------*/

    fun provideMoviesRepository(): MoviesRepository = moviesRepository

    fun provideInternetRepository(): InternetRepository = internetRepository
}