package `in`.co.example.mayanksharma_movies.presentation.movies.fragments

import `in`.co.example.mayanksharma_movies.R
import `in`.co.example.mayanksharma_movies.common.base.BaseFragment
import `in`.co.example.mayanksharma_movies.databinding.MovieDetailFragmentBinding
import `in`.co.example.mayanksharma_movies.presentation.movies.MoviesActivity
import `in`.co.example.mayanksharma_movies.presentation.movies.MoviesViewModel
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide


class MovieDetailFragment : BaseFragment<MovieDetailFragmentBinding>(
    R.layout.movie_detail_fragment
) {

    private val moviesViewModel: MoviesViewModel by activityViewModels { viewModelFactory }
    private var articleLink: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initHeader()
        val itemPosition = arguments?.getInt("position")
        itemPosition?.let {
            initData(it)
            initListener()
        }
    }

    private fun initHeader() {
        (activity as MoviesActivity).supportActionBar?.title =
            getString(R.string.movie_detail_label)
    }

    private fun initData(itemPosition: Int) {
        val movieDetail = moviesViewModel.getMovieDetail(itemPosition)
        viewBinding.apply {

            Glide.with(requireContext()).load(movieDetail.image)
                .placeholder(R.drawable.image_loading).error(R.drawable.image_not_found)
                .into(movieImage)

            tvMovieName.text = movieDetail.headLine
            tvMovieDescription.text = movieDetail.description
            tvBy.text = movieDetail.by
            tvRating.text = movieDetail.rating
            tvArticle.text = movieDetail.articleHeading.toSpanned()
            articleLink = movieDetail.articleLink
            tvArticle.movementMethod = LinkMovementMethod.getInstance()
        }
    }

    private fun initListener() {
        viewBinding.tvArticle.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(articleLink))
            startActivity(browserIntent)
        }
    }

    fun String.toSpanned() : Spanned {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            return Html.fromHtml(this)
        }
    }
}