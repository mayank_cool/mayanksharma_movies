package `in`.co.example.mayanksharma_movies

import `in`.co.example.mayanksharma_movies.common.di.DependencyProvider
import android.app.Application

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        DependencyProvider.inject(this)
    }
}