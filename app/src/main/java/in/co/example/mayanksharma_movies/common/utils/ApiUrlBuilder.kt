package `in`.co.example.mayanksharma_movies.common.utils


object ApiUrlBuilder {


    private const val MOVIES_END_POINT = "reviews/picks.json?"

    fun getMoviesUrl() = MOVIES_END_POINT
}