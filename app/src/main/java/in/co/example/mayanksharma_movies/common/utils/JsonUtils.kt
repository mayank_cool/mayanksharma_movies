package `in`.co.example.mayanksharma_movies.common.utils

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object JsonUtils {


    inline fun <reified T> convertJsonStringToObject(jsonString: String?): T? {
        if (!jsonString.isNullOrEmpty()) {
            try {
                return Gson().fromJson(jsonString, object : TypeToken<T>() {}.type)
            } catch (e: Exception) {
                Log.e("TAG", "Exception occurred while converting parsing json string to Class")
            }
        }
        return null
    }
}