package `in`.co.example.mayanksharma_movies.presentation.movies

import `in`.co.example.mayanksharma_movies.R
import `in`.co.example.mayanksharma_movies.databinding.MovieRowItemBinding
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class MoviesListAdapter(private val onMoviesItemClick: (Int) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context
    private val items: MutableList<MovieRowData> = mutableListOf()

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(MovieRowItemBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MovieViewHolder).bind(items[position], position)
    }

    override fun getItemCount(): Int = items.size

    fun addItems(data: List<MovieRowData>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }


    inner class MovieViewHolder(private val binding: MovieRowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movieRowData: MovieRowData, position: Int) {
            itemView.setOnClickListener { onMoviesItemClick(position) }
            binding.apply {
                Glide.with(context).load(movieRowData.movieImage)
                    .placeholder(R.drawable.image_loading).error(R.drawable.image_not_found)
                    .into(movieImage)
                tvMovieName.text = movieRowData.movieName
            }
        }
    }

}