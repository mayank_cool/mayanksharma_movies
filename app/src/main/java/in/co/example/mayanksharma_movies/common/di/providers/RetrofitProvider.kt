package `in`.co.example.mayanksharma_movies.common.di.providers

import `in`.co.example.mayanksharma_movies.BuildConfig
import `in`.co.example.mayanksharma_movies.data.sources.api.MoviesApiServices
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitProvider {


    private val gson: Gson = GsonBuilder().create()

    private val gsonConverterFactory: GsonConverterFactory = GsonConverterFactory.create(gson)

    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(120, TimeUnit.SECONDS)
        .build()

    private val retrofit: Retrofit =
             Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .baseUrl(BuildConfig.BASE_URL)
            .build()

    fun provideMoviesServices(): MoviesApiServices = retrofit.create(MoviesApiServices::class.java)
}