package `in`.co.example.mayanksharma_movies.presentation.movies

import `in`.co.example.mayanksharma_movies.R
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MoviesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = getString(R.string.movies_label)
    }
}