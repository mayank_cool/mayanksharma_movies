package `in`.co.example.mayanksharma_movies.common.di

import `in`.co.example.mayanksharma_movies.common.di.providers.*
import android.content.Context
import androidx.lifecycle.ViewModelProvider

object DependencyProvider {

    private lateinit var applicationContext: Context

    fun inject(context: Context) {
        applicationContext = context
    }

    /*--------------------------------------------Sources-------------------------------------------*/

    private val internetSourceProvider: InternetSourceProvider by lazy {
        InternetSourceProvider(
            provideApplicationContext()
        )
    }

    private val retrofitProvider: RetrofitProvider = RetrofitProvider()

    private val apiSourceProvider: ApiSourceProvider by lazy {
        ApiSourceProvider(retrofitProvider, authSourceProvider)
    }

    private val authSourceProvider: AuthSourceProvider by lazy {
        AuthSourceProvider()
    }

    /*--------------------------------------------Repository-------------------------------------------*/

    private val repositoryProvider: RepositoryProvider by lazy {
        RepositoryProvider(
            apiSourceProvider,
            internetSourceProvider
        )
    }


    /*--------------------------------------------Use Case-------------------------------------------*/

    private val useCaseProvider: UseCaseProvider by lazy { UseCaseProvider(repositoryProvider) }

    private val viewModelFactory: ViewModelProvider.Factory by lazy {
        ViewModelFactoryProvider(
            useCaseProvider
        )
    }


    /*--------------------------------------------Public Providers-------------------------------------------*/

    private fun provideApplicationContext() = applicationContext

    fun provideViewModelFactory(): ViewModelProvider.Factory = viewModelFactory
}