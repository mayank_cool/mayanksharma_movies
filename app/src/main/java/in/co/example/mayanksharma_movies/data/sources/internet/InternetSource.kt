@file:Suppress("DEPRECATION")

package `in`.co.example.mayanksharma_movies.data.sources.internet

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

interface InternetSource {
    fun emitInternetStatus(): Flow<Boolean>
    suspend fun getInternetStatus(): Boolean
}

@ExperimentalCoroutinesApi
class InternetSourceImpl(context: Context) : InternetSource {
    private val connectivityProvider: ConnectivityProvider
    private val channel = ConflatedBroadcastChannel<Boolean>()
    private val listener: ((NetworkState) -> Unit) = {
        handleNewState(it)
    }

    init {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityProvider = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NewConnectivityProvider(cm, listener)
        } else {
            LegacyConnectivityProvider(context, cm, listener)
        }
        handleNewState(connectivityProvider.getNetworkState())
    }

    @FlowPreview
    override fun emitInternetStatus(): Flow<Boolean> {
        return channel.asFlow()
    }

    override suspend fun getInternetStatus(): Boolean {
        return getInternetStatusBool(connectivityProvider.getNetworkState())
    }

    private fun handleNewState(state: NetworkState) {
        if (!channel.isClosedForSend) {
            channel.offer(getInternetStatusBool(state))
        }
    }

    private fun getInternetStatusBool(state: NetworkState): Boolean {
        return when (state) {
            is NetworkState.NotConnectedState -> false
            is NetworkState.ConnectedState -> state.hasInternet
        }
    }
}

interface ConnectivityProvider {
    fun getNetworkState(): NetworkState
}

sealed class NetworkState {
    object NotConnectedState : NetworkState()

    sealed class ConnectedState(val hasInternet: Boolean) : NetworkState() {

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        data class Connected(val capabilities: NetworkCapabilities) : ConnectedState(
            capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        )

        data class ConnectedLegacy(val networkInfo: NetworkInfo) : ConnectedState(
            networkInfo.isConnectedOrConnecting
        )
    }
}
