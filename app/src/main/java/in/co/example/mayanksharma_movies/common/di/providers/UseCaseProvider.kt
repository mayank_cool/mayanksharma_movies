package `in`.co.example.mayanksharma_movies.common.di.providers

import `in`.co.example.mayanksharma_movies.domain.usecases.InternetUseCase
import `in`.co.example.mayanksharma_movies.domain.usecases.InternetUseCaseImpl
import `in`.co.example.mayanksharma_movies.domain.usecases.MoviesUseCase
import `in`.co.example.mayanksharma_movies.domain.usecases.MoviesUseCaseImpl

class UseCaseProvider(
    private val repositoryProvider: RepositoryProvider
) {

   fun provideMoviesUseCase():MoviesUseCase =
       MoviesUseCaseImpl(moviesRepository = repositoryProvider.provideMoviesRepository())

    fun provideInternetUseCase():InternetUseCase =
        InternetUseCaseImpl(repositoryProvider.provideInternetRepository())
}