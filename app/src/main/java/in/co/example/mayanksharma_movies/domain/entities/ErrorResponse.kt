package `in`.co.example.mayanksharma_movies.domain.entities

data class ErrorResponse(val statusCode: Int?, val error: String?, val message:String?)