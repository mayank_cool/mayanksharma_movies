package `in`.co.example.mayanksharma_movies.domain.usecases

import `in`.co.example.mayanksharma_movies.data.repository.MoviesRepository
import `in`.co.example.mayanksharma_movies.domain.AppResult
import `in`.co.example.mayanksharma_movies.domain.entities.MoviesEntity

interface MoviesUseCase {

    suspend fun getMoviesList(): AppResult<MoviesEntity?>
}


class MoviesUseCaseImpl(private val moviesRepository: MoviesRepository) : MoviesUseCase {

    override suspend fun getMoviesList(): AppResult<MoviesEntity?> {
        return moviesRepository.getMoviesList()
    }

}