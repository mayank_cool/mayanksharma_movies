package `in`.co.example.mayanksharma_movies.common.di.providers

import `in`.co.example.mayanksharma_movies.presentation.movies.MoviesViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class ViewModelFactoryProvider(
    private val useCaseProvider: UseCaseProvider
) : ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        when {
            modelClass.isAssignableFrom(MoviesViewModel::class.java) -> {
                return MoviesViewModel(
                    useCaseProvider.provideMoviesUseCase(),
                    useCaseProvider.provideInternetUseCase()
                ) as T
            }

            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}