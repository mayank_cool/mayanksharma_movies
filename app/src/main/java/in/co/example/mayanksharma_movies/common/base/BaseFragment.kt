package `in`.co.example.mayanksharma_movies.common.base

import `in`.co.example.mayanksharma_movies.common.di.DependencyProvider
import `in`.co.example.mayanksharma_movies.databinding.SnackErrorViewBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<B : ViewDataBinding>(
    @LayoutRes private val layoutId: Int
) : Fragment() {

    private var _viewBinding: B? = null

    protected val viewModelFactory = DependencyProvider.provideViewModelFactory()

    val viewBinding get() = _viewBinding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _viewBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        viewBinding.lifecycleOwner = viewLifecycleOwner
        return viewBinding.root
    }

    protected open fun getSnackBarErrorView(): SnackErrorViewBinding? = null

    protected fun showSnackBarErrorView(message: String) {
        getSnackBarErrorView()?.apply {
            root.visibility = View.VISIBLE
            tvWarning.text = message
        }
    }

    protected fun hideSnackBarErrorView() {
        getSnackBarErrorView()?.root?.visibility = View.GONE
    }
}