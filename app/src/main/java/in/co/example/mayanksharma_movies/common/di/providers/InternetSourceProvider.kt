package `in`.co.example.mayanksharma_movies.common.di.providers

import `in`.co.example.mayanksharma_movies.data.sources.internet.InternetSource
import `in`.co.example.mayanksharma_movies.data.sources.internet.InternetSourceImpl
import android.content.Context

class InternetSourceProvider(context: Context) {

    private val internetSource: InternetSource = InternetSourceImpl(context)
    fun provideInternetSource(): InternetSource = internetSource
}