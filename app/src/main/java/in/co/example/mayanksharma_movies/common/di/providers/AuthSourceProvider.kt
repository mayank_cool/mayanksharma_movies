package `in`.co.example.mayanksharma_movies.common.di.providers

import `in`.co.example.mayanksharma_movies.data.sources.auth.AuthSource
import `in`.co.example.mayanksharma_movies.data.sources.auth.AuthSourceImpl

class AuthSourceProvider {

    private val authSource: AuthSource = AuthSourceImpl()

    fun provideAuthSource(): AuthSource = authSource

}