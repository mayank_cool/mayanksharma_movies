package `in`.co.example.mayanksharma_movies.data.sources.api

import `in`.co.example.mayanksharma_movies.common.utils.ApiUrlBuilder
import `in`.co.example.mayanksharma_movies.data.ErrorMapper
import `in`.co.example.mayanksharma_movies.data.sources.auth.AuthSource
import `in`.co.example.mayanksharma_movies.domain.AppResult
import `in`.co.example.mayanksharma_movies.domain.RetrofitResult
import `in`.co.example.mayanksharma_movies.domain.entities.MoviesEntity
import retrofit2.Response

interface MoviesApi {
    suspend fun getMoviesList(): AppResult<MoviesEntity?>
}

class MoviesApiImpl(
    private val moviesApiServices: MoviesApiServices,
    private val authSource: AuthSource
) : MoviesApi {

    override suspend fun getMoviesList(): AppResult<MoviesEntity?> {
        var response: Response<MoviesEntity>? = null
        var exception: Exception? = null
        val apiKey = authSource.getApiKey()
        val queryMap: MutableMap<String, String> = mutableMapOf()
        queryMap["api-key"] = apiKey

        try {
            response = moviesApiServices.getMovies(
                ApiUrlBuilder.getMoviesUrl(),
                queryMap
            )
        } catch (e: Exception) {
            exception = e
        }

        return when (val result = ErrorMapper.checkAndMapError(response, exception)) {

            is RetrofitResult.Success -> {
                AppResult.Success(result.data)
            }

            is RetrofitResult.Failure -> AppResult.Failure(result.error)
        }
    }

}