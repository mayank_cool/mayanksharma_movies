package `in`.co.example.mayanksharma_movies.data.sources.auth

import `in`.co.example.mayanksharma_movies.domain.AppResult

interface AuthSource {

    suspend fun getApiKey(): String
}

class AuthSourceImpl : AuthSource {

    override suspend fun getApiKey(): String {
        return "HiiVbL5wDkdP1NJQpIUCjZrHW4P0SDqG"
    }

}