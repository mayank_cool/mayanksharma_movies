package `in`.co.example.mayanksharma_movies.domain.entities

import com.google.gson.annotations.SerializedName

data class MoviesEntity(
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("copyright")
    val copyright: String? = null,
    @SerializedName("has_more")
    val hasMore: Boolean,
    @SerializedName("num_results")
    val num_results: Int? = 0,
    @SerializedName("results")
    val movies: List<Movie>? = null
)

data class Movie(
    @SerializedName("display_title")
    val displayTitle: String? = null,
    @SerializedName("mpaa_rating")
    val rating: String? = null,
    @SerializedName("critics_pick")
    val criticsPick: Int? = 0,
    @SerializedName("byline")
    val byLine: String? = null,
    @SerializedName("headline")
    val headLine: String? = null,
    @SerializedName("summary_short")
    val summaryShort: String? = null,
    @SerializedName("publication_date")
    val publicationDate: String? = null,
    @SerializedName("opening_date")
    val openingDate: String? = null,
    @SerializedName("date_updated")
    val dateUpdated: String? = null,
    @SerializedName("link")
    val link: Link? = null,
    @SerializedName("multimedia")
    val multimedia: MultiMedia? = null
)

data class Link(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("suggested_link_text")
    val suggestedLinkText: String? = null
)

data class MultiMedia(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("src")
    val src: String? = null,
    @SerializedName("height")
    val height: Int? = 0,
    @SerializedName("width")
    val width: Int? = 0,
)
