package `in`.co.example.mayanksharma_movies.domain.usecases

import `in`.co.example.mayanksharma_movies.data.repository.InternetRepository
import kotlinx.coroutines.flow.Flow

interface InternetUseCase{
    fun receiveInternetStatus(): Flow<Boolean>
}

class InternetUseCaseImpl(private val internetRepository: InternetRepository):InternetUseCase{

    override fun receiveInternetStatus(): Flow<Boolean> {
        return internetRepository.receiveInternetStatus()
    }
}