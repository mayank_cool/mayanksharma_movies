package `in`.co.example.mayanksharma_movies.domain

sealed class SnackErrorType {
    data class InternetError(val isInternet: Boolean) : SnackErrorType()
    data class ServerError(val msg: String) : SnackErrorType()
}