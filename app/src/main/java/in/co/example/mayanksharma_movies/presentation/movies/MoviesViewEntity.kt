package `in`.co.example.mayanksharma_movies.presentation.movies


data class MovieRowData(
    val movieName: String,
    val rating: String,
    val movieImage: String? = null
)

data class MovieDetail(
    val name: String,
    val headLine: String,
    val description: String,
    val rating: String,
    val by: String,
    val havingArticle: Boolean,
    val articleLink: String,
    val articleHeading: String,
    val image: String
)