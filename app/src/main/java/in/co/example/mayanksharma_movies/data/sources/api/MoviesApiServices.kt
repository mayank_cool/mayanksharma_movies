package `in`.co.example.mayanksharma_movies.data.sources.api

import `in`.co.example.mayanksharma_movies.domain.entities.MoviesEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap
import retrofit2.http.Url

interface MoviesApiServices {

    @GET
    suspend fun getMovies(
        @Url url: String,
        @QueryMap map: Map<String, String>
    ): Response<MoviesEntity>

}