package `in`.co.example.mayanksharma_movies.common.di.providers

import `in`.co.example.mayanksharma_movies.data.sources.api.MoviesApi
import `in`.co.example.mayanksharma_movies.data.sources.api.MoviesApiImpl

class ApiSourceProvider(
    private val retrofitProvider: RetrofitProvider,
    private val authSourceProvider: AuthSourceProvider
) {

    fun provideMoviesApi(): MoviesApi =
        MoviesApiImpl(
            retrofitProvider.provideMoviesServices(),
            authSourceProvider.provideAuthSource()
        )
}