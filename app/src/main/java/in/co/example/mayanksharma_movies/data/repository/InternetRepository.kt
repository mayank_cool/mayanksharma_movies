package `in`.co.example.mayanksharma_movies.data.repository

import `in`.co.example.mayanksharma_movies.data.sources.internet.InternetSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

interface InternetRepository {
    fun receiveInternetStatus(): Flow<Boolean>
}

class InternetRepositoryImpl(
    private val internetSource: InternetSource,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : InternetRepository {

    override fun receiveInternetStatus(): Flow<Boolean> {
        return internetSource.emitInternetStatus().flowOn(dispatcher)
    }
}